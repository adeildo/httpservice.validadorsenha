﻿using HttpService.ValidadorSenha.Models;
using System.Net;

namespace HttpService.ValidadorSenha.Controllers;

public abstract class BaseApiController : ControllerBase
{
    protected new IActionResult Response(bool result)
    {
        var bodyMessage = new ResponseMensage();

        if (result)
        {
            bodyMessage.StatusCode = (int)HttpStatusCode.OK;
            bodyMessage.Result = result;
        }
        else
        {
            bodyMessage.StatusCode = (int)HttpStatusCode.BadRequest;
            bodyMessage.Result = result;
        }

        return StatusCode(bodyMessage.StatusCode, bodyMessage);
    }
}
