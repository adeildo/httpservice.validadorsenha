﻿using HttpService.ValidadorSenha.Models;
using HttpService.ValidadorSenha.ServicoValidaSenha;

namespace HttpService.ValidadorSenha.Controllers.V1;

[Route("api/[controller]")]
[ApiVersion("1", Deprecated = false)]
[ApiController]
public class ValidaSenhaController : BaseApiController
{
    private readonly IServicoValidacaoSenha _servicoValidacaoSenha;

    public ValidaSenhaController(IServicoValidacaoSenha servicoValidacaoSenha) => _servicoValidacaoSenha = servicoValidacaoSenha;

    [HttpPost]
    [Route("ValidarSenha")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Post([FromBody] ValidaSenhaModel validaSenha)
    {
        var resultado = _servicoValidacaoSenha.SenhaEhValida(validaSenha.Senha);
        return Response(resultado);
    }
}
