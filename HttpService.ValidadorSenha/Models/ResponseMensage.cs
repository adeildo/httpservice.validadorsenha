﻿namespace HttpService.ValidadorSenha.Models;

public struct ResponseMensage
{
    public int StatusCode { get; set; }
    public object Result { get; set; }
    public object Erros { get; set; }
}
