﻿namespace HttpService.ValidadorSenha.Models;

public struct ErroMessage
{
    public ErroMessage(string key, string value)
    {
        Key = key;
        Value = value;
    }

    public string? Key { get; private set; }
    public string? Value { get; private set; }
}