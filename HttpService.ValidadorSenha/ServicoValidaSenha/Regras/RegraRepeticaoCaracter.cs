﻿namespace HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

public class RegraRepeticaoCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request)
    {
        var caracteresOrdenados = request.ToLower().OrderBy(x => x).ToArray();
        var length = caracteresOrdenados.Length;
        var possuiRepeticao = true;
        var esquerda = calculaInteracoesEsquerda(length);
        var direita = calculaIteracoesDireita(length, esquerda);
        var proximo = 1;
        var anterior = length - 1;

        for (var i = 0; i < length; i++)
        {
            LendoDaEsquerdaParaDireita(caracteresOrdenados, ref possuiRepeticao, esquerda, ref proximo, i);
            LendoDaDireitaParaEsquerda(caracteresOrdenados, ref possuiRepeticao, direita, ref anterior);

            if (possuiRepeticao is false)
                return possuiRepeticao;

            if (proximo == esquerda && direita == anterior)
                return possuiRepeticao;
        }

        return possuiRepeticao;
    }

    private static int calculaInteracoesEsquerda(int length) => length - (length / 2);

    private static int calculaIteracoesDireita(int length, int esquerda) => length % 2 == 0 ? (length - esquerda) - 1 : length - esquerda;

    private static void LendoDaEsquerdaParaDireita(char[] caracteresOrdenados, ref bool possuiRepeticao, int esquerda, ref int proximo, int i)
    {
        if (proximo < esquerda && possuiRepeticao)
        {
            possuiRepeticao = caracteresOrdenados[i] != caracteresOrdenados[proximo];
            proximo++;
        }
    }

    private static void LendoDaDireitaParaEsquerda(char[] caracteresOrdenados, ref bool possuiRepeticao, int direita, ref int anterior)
    {
        if (direita < anterior && possuiRepeticao)
        {
            possuiRepeticao = caracteresOrdenados[anterior] != caracteresOrdenados[anterior - 1];
            anterior--;
        }
    }
}