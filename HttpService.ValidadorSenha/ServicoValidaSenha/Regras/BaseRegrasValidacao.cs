﻿namespace HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

public abstract class BaseRegrasValidacao
{
    protected const int TamanhoMinimoSenha = 9;
    protected static char[] CaractersEspeciais = { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+' };
}

public interface IRegrasValidacao
{
    bool ExecutarRegra(string request);
}