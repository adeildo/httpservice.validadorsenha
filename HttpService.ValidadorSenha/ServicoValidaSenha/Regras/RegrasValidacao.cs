﻿namespace HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

public class RegraLetraMaisculaCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Any(x => char.IsUpper(x));
}

public class RegraLetraMinisculaCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Any(x => char.IsLower(x));
}

public class RegraEspacoCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Any(x => char.IsWhiteSpace(x));
}

public class RegraDigitoCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Any(x => char.IsDigit(x));
}

public class RegraEspecialCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Any(x => CaractersEspeciais.Any(p => p == x));
}

public class RegraLimiteMinimoCaracter : BaseRegrasValidacao, IRegrasValidacao
{
    public bool ExecutarRegra(string request) => request.Length >= TamanhoMinimoSenha;
}
