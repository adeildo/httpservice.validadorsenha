﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace HttpService.ValidadorSenha.ServicoValidaSenha;

public interface IServicoValidacaoSenha
{
    bool SenhaEhValida(string request);
}

public class ServicoValidacaoSenha : IServicoValidacaoSenha
{
    private readonly IEnumerable<IRegrasValidacao> _regrasValidacao;

    public ServicoValidacaoSenha(IEnumerable<IRegrasValidacao> regrasValidacao) => _regrasValidacao = regrasValidacao;

    public bool SenhaEhValida(string request)
    {
        if (string.IsNullOrWhiteSpace(request))
            return false;

        return _regrasValidacao.Any(x => x.ExecutarRegra(request) is false) ? false : true;
    }
}