﻿using HttpService.ValidadorSenha.ServicoValidaSenha;
using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;
using HttpService.ValidadorSenha.SwaggerOptions;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace HttpService.ValidadorSenha;

public static class NativeInjectorBootStrapper
{
    public static void RegisterServices(this IServiceCollection services) => services.Services();

    public static void SwaggerRegisterServices(this IServiceCollection services)
    {
        services.AddApiVersioning(p =>
        {
            p.DefaultApiVersion = new ApiVersion(1, 0);
            p.ReportApiVersions = true;
            p.AssumeDefaultVersionWhenUnspecified = true;
        });

        services.AddVersionedApiExplorer(p =>
        {
            p.GroupNameFormat = "'v'VVV";
            p.SubstituteApiVersionInUrl = true;
        });

        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
        services.AddSwaggerGen();
    }

    private static void Services(this IServiceCollection services)
    {
        services.AddScoped<IServicoValidacaoSenha, ServicoValidacaoSenha>();
        services.AddScoped<IRegrasValidacao, RegraRepeticaoCaracter>();
        services.AddScoped<IRegrasValidacao, RegraDigitoCaracter>();
        services.AddScoped<IRegrasValidacao, RegraEspecialCaracter>();
        services.AddScoped<IRegrasValidacao, RegraLetraMaisculaCaracter>();
        services.AddScoped<IRegrasValidacao, RegraLetraMinisculaCaracter>();
        services.AddScoped<IRegrasValidacao, RegraLimiteMinimoCaracter>();
    }
}
