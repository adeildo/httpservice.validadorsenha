using HttpService.ValidadorSenha;
using HttpService.ValidadorSenha.CustomExtensions;
using Microsoft.AspNetCore.Mvc.Formatters;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Environment.ConfigurationExtension();

builder.WebHost.UseConfiguration(configuration);
builder.Services.RegisterServices();
builder.Services.SwaggerRegisterServices();

builder.Services
    .AddControllers(options => options.OutputFormatters.RemoveType<StringOutputFormatter>())
    .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null)// Use the default property (Pascal) casing.
    .ConfigureApiBehaviorOptions(options =>
    {
        options.SuppressMapClientErrors = true;
        options.InvalidModelStateResponseFactory = context => context.CustomBadRequestResultExtension();
    });

builder.Services.AddEndpointsApiExplorer();
builder.Build().WebApplicationExtension();