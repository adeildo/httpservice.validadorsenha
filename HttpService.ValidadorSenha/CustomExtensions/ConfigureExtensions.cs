﻿using HttpService.ValidadorSenha.Models;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Net;
using System.Net.Mime;

namespace HttpService.ValidadorSenha.CustomExtensions;

public static class ConfigureExtensions
{
    public static void WebApplicationExtension(this WebApplication app)
    {
        var provider = app.Services.GetService<IApiVersionDescriptionProvider>();

        if (!app.Environment.IsDevelopment())
        {
            app.UseHsts();
            app.UseHttpsRedirection();
        }

        app.UseGlobalExceptionHandler();
        app.UseAuthorization();
        app.MapControllers();
        app.UseSwagger(c => c.SerializeAsV2 = true);
        app.UseSwaggerUI(options =>
        {
            for (int i = 0; i < provider.ApiVersionDescriptions.Count; i++)
            {
                ApiVersionDescription? description = provider.ApiVersionDescriptions[i];
                options.SwaggerEndpoint(
                $"/swagger/{description.GroupName}/swagger.json",
                description.GroupName.ToUpperInvariant());
            }

            options.DocExpansion(DocExpansion.List);
        });
        app.Run();
    }

    public static IConfiguration ConfigurationExtension(this IWebHostEnvironment hostEnvironment) => hostEnvironment.EnvironmentName switch
    {
        "Development" => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Development.json").Build(),
        "Staging" => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Staging.json").Build(),
        "Tests" => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.Tests.json").Build(),
        _ => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build()
    };

    public static BadRequestObjectResult CustomBadRequestResultExtension(this ActionContext context)
    {
        var erros = new HashSet<ErroMessage>();

        foreach (var item in context.ModelState)
        {
            foreach (var erroMessage in item.Value.Errors)
            {
                erros.Add(new ErroMessage(item.Key, erroMessage.ErrorMessage));
            }
        }

        var result = new BadRequestObjectResult(new ResponseMensage
        {
            Erros = erros,
            StatusCode = (int)HttpStatusCode.BadRequest
        });

        result.ContentTypes.Add(MediaTypeNames.Application.Json);

        return result;
    }
}
