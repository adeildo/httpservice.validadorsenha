﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraEspacoCaracterTests
{
    private readonly RegraEspacoCaracter _regra = new();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoNaoHouverEspaco")]
    [InlineData("TeS@ABcW")]
    [InlineData("TEs@CDFw")]
    public void DeveRetornarFalsoQuandoNaoHouverEspaco(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverEspaco")]
    [InlineData("tes@abcw ")]
    [InlineData("te s@cdfw")]
    public void DeveRetornarVerdadeiroQuandoHouverEspaco(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();
}