﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraDigitoCaracterTests
{
    private readonly RegraDigitoCaracter _regra = new();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverDigito")]
    [InlineData("tes@305w9")]
    [InlineData("tes@305j9")]
    public void DeveRetornarVerdadeiroQuandoHouverDigito(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoNaoHouverDigito")]
    [InlineData("tes@asdfwá")]
    [InlineData("tes@qwerjç")]
    public void DeveRetornarFalsoQuandoNaoHouverDigito(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}
