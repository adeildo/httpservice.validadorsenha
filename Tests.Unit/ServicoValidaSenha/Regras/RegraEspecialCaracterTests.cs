﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraEspecialCaracterTests
{
    private readonly RegraEspecialCaracter _regra = new();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverCaracterEspecial")]
    [InlineData("TeS@ABcW")]
    [InlineData("TEs#CDFw")]
    public void DeveRetornarVerdadeiroQuandoHouverCaracterEspecial(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoNaoHouverCaracterEspecial")]
    [InlineData("TES5ABCW")]
    [InlineData("TES6CDFWç")]
    [InlineData("TES6CDFW/")]
    public void DeveRetornarFalsoQuandoNaoHouverCaracterEspecial(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}
