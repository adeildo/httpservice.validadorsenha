﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraRepeticaoCaracterTests
{
    private readonly RegraRepeticaoCaracter _regra = new ();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoSenhaNaoHouverRepeticao")]
    [InlineData("tes@307")]
    [InlineData("TesPi@314")]
    public void DeveRetornarVerdadeiroQuandoSenhaNaoHouverRepeticao(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoSenhaHouverRepeticao")]
    [InlineData("abccde")]
    [InlineData("Teetotp8a")]
    [InlineData("TTeste@1234")]
    [InlineData("tTeste@1234")]
    public void DeveRetornarFalsoQuandoSenhaHouverRepeticao(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}
