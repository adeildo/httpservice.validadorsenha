﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraLetraMaisculaCaracterTests
{
    private readonly RegraLetraMaisculaCaracter _regra = new();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverLestrasMaisculas")]
    [InlineData("TeS@ABcW")]
    [InlineData("TEs@CDFw")]
    public void DeveRetornarVerdadeiroQuandoHouverLestrasMaisculas(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoNaoHouverLestrasMaisculas")]
    [InlineData("tes@abcw")]
    [InlineData("tes@cdfw")]
    public void DeveRetornarFalsoQuandoNaoHouverLestrasMinisculas(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}