﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraLimiteMinimoCaracterTests
{
    private readonly RegraLimiteMinimoCaracter _regra = new ();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverNoveOuMaisCaracteres")]
    [InlineData("tes@305w9")]
    [InlineData("tes@305j9")]
    public void DeveRetornarVerdadeiroQuandoHouverNoveOuMaisCaracteres(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoHouverMenosDeNovoCarater")]
    [InlineData("tes@305w")]
    [InlineData("tes@385w")]
    public void DeveRetornarFalsoQuandoHouverMenosDeNovoCarater(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}
