﻿using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha.Regras;

public class RegraLetraMinisculaCaracterTests
{
    private readonly RegraLetraMinisculaCaracter _regra = new();

    [Theory(DisplayName = "DeveRetornarVerdadeiroQuandoHouverLestrasMinisculas")]
    [InlineData("TeS@ABcW")]
    [InlineData("TEs@CDFw")]
    public void DeveRetornarVerdadeiroQuandoHouverLestrasMinisculas(string parameter) => _regra.ExecutarRegra(parameter).Should().BeTrue();

    [Theory(DisplayName = "DeveRetornarFalsoQuandoNaoHouverLestrasMinisculas")]
    [InlineData("TES@ABCW")]
    [InlineData("TES@CDFW")]
    public void DeveRetornarFalsoQuandoNaoHouverLestrasMaisculas(string parameter) => _regra.ExecutarRegra(parameter).Should().BeFalse();
}
