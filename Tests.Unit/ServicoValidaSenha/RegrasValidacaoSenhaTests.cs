﻿using HttpService.ValidadorSenha.ServicoValidaSenha;
using HttpService.ValidadorSenha.ServicoValidaSenha.Regras;

namespace Tests.Unit.ServicoValidaSenha;

public class RegrasValidacaoSenhaTests
{
    private readonly Mock<IRegrasValidacao> _regrasValidacaos;
    private readonly ServicoValidacaoSenha _servicoValidacaosSenha;

    public RegrasValidacaoSenhaTests()
    {
        var mock = new AutoMocker();

        _regrasValidacaos = mock.GetMock<IRegrasValidacao>();
        _servicoValidacaosSenha = mock.CreateInstance<ServicoValidacaoSenha>();
    }

    [Fact(DisplayName = "DeveRetornarVerdadeiroQuandoValidadoASenha")]
    public void DeveRetornarVerdadeiroQuandoValidadoASenha()
    {
        var texto = "tes@305j9";
        _regrasValidacaos.Setup(x => x.ExecutarRegra(texto)).Returns(true);

        var resultado = _servicoValidacaosSenha.SenhaEhValida(texto);

        resultado.Should().BeTrue();
    }

    [Fact(DisplayName = "DeveRetornarFalsoQuandoValidadoASenha")]
    public void DeveRetornarFalsoQuandoValidadoASenha()
    {
        var texto = "tes@305j9";
        _regrasValidacaos.Setup(x => x.ExecutarRegra(texto)).Returns(false);

        var resultado = _servicoValidacaosSenha.SenhaEhValida(texto);

        resultado.Should().BeFalse();
    }

    [Theory(DisplayName = "DeveRetornarFalsoTextoEnviadoForVazioOuNulo")]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("   ")]
    public void DeveRetornarFalsoTextoEnviadoForVazioOuNulo(string texto)
    {
        _regrasValidacaos.Setup(x => x.ExecutarRegra(texto)).Returns(false);

        var resultado = _servicoValidacaosSenha.SenhaEhValida(texto);

        resultado.Should().BeFalse();
    }
}
