﻿using HttpService.ValidadorSenha.Controllers.V1;
using HttpService.ValidadorSenha.Models;
using HttpService.ValidadorSenha.ServicoValidaSenha;
using Microsoft.AspNetCore.Mvc;

namespace Tests.Unit.API;

public class ValidaSenhaControllerTests
{
    private readonly ValidaSenhaController _controller;
    private readonly Mock<IServicoValidacaoSenha> _servicoValidacaoSenha;

    public ValidaSenhaControllerTests()
    {
        var mock = new AutoMocker();
        _servicoValidacaoSenha = mock.GetMock<IServicoValidacaoSenha>();
        _controller = mock.CreateInstance<ValidaSenhaController>();
    }

    [Fact(DisplayName = "DeveRetornarStatusCode200QuandoHouverSucessoNaValidacaoSenha")]
    public void DeveRetornarStatusCode200QuandoHouverSucessoNaValidacaoSenha()
    {
        var request = new ValidaSenhaModel { Senha = "Teste@1234" };
        _servicoValidacaoSenha.Setup(x => x.SenhaEhValida(It.IsAny<string>())).Returns(true);

        var viewResult = (_controller.Post(request)) as ObjectResult;

        viewResult.StatusCode.Should().Be(200);
        viewResult.Should().BeOfType<ObjectResult>();
    }

    [Fact(DisplayName = "DeveRetornarStatusCode400QuandoNaoHouverSucessoNaValidacaoSenha")]
    public void DeveRetornarStatusCode400QuandoNaoHouverSucessoNaValidacaoSenha()
    {
        var request = new ValidaSenhaModel { Senha = "Teste@1234" };
        _servicoValidacaoSenha.Setup(x => x.SenhaEhValida(It.IsAny<string>())).Returns(false);

        var viewResult = (_controller.Post(request)) as ObjectResult;

        viewResult.StatusCode.Should().Be(400);
        viewResult.Should().BeOfType<ObjectResult>();
    }
}
