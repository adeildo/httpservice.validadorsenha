# API Validador de Senha
Esta api construída em **.Net Core 6** visita exemplificar a valiação de senha. Para isso, foi contruído um endpoint (url) com verbo **Post** no intuido de devolver respostas boolean.

Não foi implementado processos de autententicação. No postman ou no swagger é possível realizar os devidos testes.

**True** para senhas válidas e **False** para senhas inválidas.

### Premissas
* Repetição de caracteres será convertido para minúsculo antes da validação;
* Os caracteres validados como especial não abrangem acentuações; e
* A execução das regras ocorrerá de forma sequêncial.

### Exemplo Request

http://localhost:80/api/ValidaSenha/ValidarSenha
```
{
  "Senha": "P@25d g7o9"
}
```
### Resultado Bad Request:
```
{
    "StatusCode": 400,
    "Result": false,
    "Erros": null
}
```
### Resultado Ok:
```
{
    "StatusCode": 200,
    "Result": true,
    "Erros": null
}
```
### No projeto, entrando na pasta aonde está o arquivo **Dockerfile**, executar os comandos abaixo:

1. Buid da imagem:
    ```
    docker build -t api-valida-senha .
    ```
2. Criação do container a partir da imagem:
    ```
    docker run -d -p 8080:80 --name api-validador-senha api-valida-senha
    ```
